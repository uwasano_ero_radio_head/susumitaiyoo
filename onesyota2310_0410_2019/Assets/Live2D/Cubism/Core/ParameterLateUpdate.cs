﻿using UnityEngine;
using Live2D.Cubism.Core;
using Live2D.Cubism.Framework;
using System.Collections.Generic;
public class ParameterLateUpdate : MonoBehaviour
{
    [System.NonSerialized]
    private CubismModel _model;
    [System.NonSerialized]
    private CubismParameter _paramAngleZ;

    //[SerializeField]
    [System.NonSerialized]
    public string ParameterID = "PARAM_ANGLE_Zge";

    public List<CubismParameter> CubismParameterList = new List<CubismParameter>();    //CubismParameter型のリスト
    public List<CubismPart> CubismPartList = new List<CubismPart>();   

    public List<CubismDrawable> CubismDrawablesList = new List<CubismDrawable>();

    public List<GameObject> CubismGameObjectList = new List<GameObject>();

    private void Start()
    {
        _model = this.FindCubismModel();

        //_paramAngleZ = _model.Parameters.FindById("PARAM_ANGLE_Z");


        //パラメータがいくつあるか取得する必要がある
      
              Debug.Log(_model.name+"のパラメータの数= " + _model.Parameters.Length);
        for (var i = 0; i < _model.Parameters.Length; i++)
        {
            CubismParameterList.Add(_model.Parameters[i]);
        }

        for (var i = 0; i < _model.Drawables.Length; i++)
        {
            CubismDrawablesList.Add(_model.Drawables[i]);
        }

        for (var i = 0; i < _model.Drawables.Length; i++)
        {
            CubismGameObjectList.Add(_model.Drawables[i].gameObject);
        }
        //結局番号管理が楽なのか？？？
        //１００個以上も名前作るのさすがに面倒か？

        _paramAngleZ = _model.Parameters.FindById(ParameterID);


        CubismGameObjectList[0].SetActive(false);
        //CubismParameterList("PARAM_ANGLE_X") = 30;
        //CubismParameter CubismParameter = "Osaka";
        //int num = CubismParameterList.IndexOf("PARAM_ANGLE_X") + 1;

        //CubismParameterList.Add(_model.Parameters[0]);//スタート画面に戻ってこられるように


        //string pName = _model.Parameters[3].name;//名前取得できる
        //Debug.Log("pName= " + pName);
        //_paramAngleZ = _model.Parameters.f
    }

   

}
