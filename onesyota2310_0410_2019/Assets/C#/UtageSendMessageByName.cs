﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using Utage;
using System.Collections;
using System;

public class UtageSendMessageByName : MonoBehaviour
{
    //宴系命令のみをここに描く
    GameObject SaveOrLoadG;
    SaveOrLoad SaveOrLoadSSS;

    GameObject UtageSeigyoG;
    UtageSeigyo UtageSeigyoSSS;


    GameObject NameButtonZentaiG;
    NameButtonZentai NameButtonZentaiSSS;//

    public bool kakuninZumi;
    [System.NonSerialized]
    public int Sam_a;//金
    [System.NonSerialized]
    public int Sam_b;//エネルギー
    [System.NonSerialized]
    public int Sam_c;//精液
    [System.NonSerialized]
    public int Sam_d;//経験値


    GameObject SaveLoadOnlyG;
    SaveLoadOnly SaveLoadOnlySSS;



    void Start()
    {

        if (!SaveOrLoadG)
        {
            SaveOrLoadG = GameObject.Find("SaveOrLoad");
            SaveOrLoadSSS = SaveOrLoadG.GetComponent<SaveOrLoad>();
        }


        if (!SaveLoadOnlyG)
        {
            SaveLoadOnlyG = GameObject.Find("SaveLoadOnly");
            SaveLoadOnlySSS = SaveLoadOnlyG.GetComponent<SaveLoadOnly>();
        }

        if (!UtageSeigyoG)//少し面倒か？　最低限必要か？
        {
            UtageSeigyoG = GameObject.Find("UtageSeigyo");
            UtageSeigyoSSS = UtageSeigyoG.GetComponent<UtageSeigyo>();
        }

        if (!NameButtonZentaiG)//少し面倒か？　最低限必要か？
        {//NameButtonZentai
            NameButtonZentaiG = GameObject.Find("NameButtonZentai");//NameButtonZentai
            NameButtonZentaiSSS = NameButtonZentaiG.GetComponent<NameButtonZentai>();
        }
    }



    void kaiwaBonusSyokika()//初期化する
    {
        Sam_a = 0;
        Sam_b = 0;
        Sam_c = 0;
        Sam_d = 0;
    }

    void simpleHanbetuSave()
    {
        //stringSaveOrLoad = ES2.Load<string>("stringSaveOrLoad?tag=string");//セーブかロードの区別
        ES2.Save("save", "stringSaveOrLoad?tag=string");
        SaveOrLoadSSS.simple();
    }
    void simpleHanbetuLoad()
    {
        ES2.Save("load", "stringSaveOrLoad?tag=string");
        SaveOrLoadSSS.simple();
    }

    void GetUtage_P(AdvCommandSendMessageByName command)//SampleSendMessageByName
    {//↑の部分の名前を間違えていた。綴りに注意。0758_0618_2018
        //kaiwaBonusSyokika();//初期化する←これ必要ないのかもな…
        //
        Sam_a = command.ParseCellOptional<int>(AdvColumnName.Arg3, 0);//偉い！！
        Sam_b = command.ParseCellOptional<int>(AdvColumnName.Arg4, 0);//エネルギー
        Sam_c = command.ParseCellOptional<int>(AdvColumnName.Arg5, 0);//精液
        Sam_d = command.ParseCellOptional<int>(AdvColumnName.Arg6, 0);//経験値 
        SaveLoadOnlySSS.packet.intItems[0] += Sam_a;//eraiPに入ってる
        SaveLoadOnlySSS.packet.intItems[1] += Sam_b;//エネルギー？
        SaveLoadOnlySSS.packet.intItems[2] += Sam_c;//精液Ｐ？//313?331?
        SaveLoadOnlySSS.packet.intItems[3] += Sam_d;//経験値
    }

    void kaneUP5000()//SampleSendMessageByName
    {

    }

    void DoSaveLoad()//これを宴で実行するとセーブされるようにする
    {
        Debug.Log("宴での確認後　これが実行される"+ NameButtonZentaiSSS.myName);
        kakuninZumi = true;
    }
    void DoSaveLoadNo()//これを宴で実行するとセーブされるようにする
    {
        Debug.Log("確認に対してNoを選んだ場合" + NameButtonZentaiSSS.myName);
        kakuninZumi = false;
        NameButtonZentaiSSS.myName = "";//myNameを初期化
        NameButtonZentaiSSS.saveAndLoadMode = false;
    }

    void utageSave()
    {

        //nt=UtageSeigyoSSS.engineUtage.Param.GetParameter("eraiP");//できたい

        SaveLoadOnlySSS.packet.intItems[0]+=0;
        UtageSeigyoSSS.engineUtage.Param.TrySetParameter("eraiP", SaveLoadOnlySSS.packet.intItems[0]);//成功した  


    }
    void go_start()
    {
        NameButtonZentaiSSS.GoStart();
        Debug.Log("go_sean1 a");
    }
    void go_sean1()
    {
        //UtageSeigyoSSS.engineUtage.UiManager.IsInputTrigCustom = tru
        Debug.Log("宴");
    }



    public void hidukeSave()//クリック時専用？？？
    {
        Debug.Log("エラーどこか１");
        //セーブモードのときに日付を上書きする
        DateTime today = DateTime.Today;
        DateTime now = DateTime.Now;

        int year = now.Year;                //　年
        int month = now.Month;              //　月
        int day = now.Day;                  //　日
        int hour = now.Hour;                //　時
        int minute = now.Minute;            //　分
        int second = now.Second;            //　秒
                                            //int millisecond = now.Millisecond;  //　1/100秒
      
        SaveLoadOnlySSS.packet.floatItems[0] = year;
        SaveLoadOnlySSS.packet.floatItems[1] = month;
        SaveLoadOnlySSS.packet.floatItems[2] = day;
        SaveLoadOnlySSS.packet.floatItems[3] = hour;
        SaveLoadOnlySSS.packet.floatItems[4] = minute;
        SaveLoadOnlySSS.packet.floatItems[5] = second;
     
        UtageSeigyoSSS.engineUtage.Param.TrySetParameter("year", SaveLoadOnlySSS.packet.floatItems[0]);
        UtageSeigyoSSS.engineUtage.Param.TrySetParameter("month", SaveLoadOnlySSS.packet.floatItems[1]);
        UtageSeigyoSSS.engineUtage.Param.TrySetParameter("day", SaveLoadOnlySSS.packet.floatItems[2]);
        UtageSeigyoSSS.engineUtage.Param.TrySetParameter("hour", SaveLoadOnlySSS.packet.floatItems[3]);
        UtageSeigyoSSS.engineUtage.Param.TrySetParameter("minute", SaveLoadOnlySSS.packet.floatItems[4]);
        UtageSeigyoSSS.engineUtage.Param.TrySetParameter("second", SaveLoadOnlySSS.packet.floatItems[5]);

      
        //ボタンの名前はどこから取得する？
        ES2.Save(SaveLoadOnlySSS.packet.floatItems, NameButtonZentaiSSS.myName);//各種名前のセーブデータゲット
      
        SaveLoadOnlySSS.packet.floatItems = ES2.LoadArray<float>(NameButtonZentaiSSS.myName);
        //これはボタンの名前があってはじめて完結する？
        //名前が無い状態だからエラーだったのかよ…
        //ファンクションあんま意味ないかもな。こうやってやるほうがいいかもしれん…

        Debug.Log("セーブデータ＝"+ SaveLoadOnlySSS.packet.floatItems[0]);
        Debug.Log("セーブデータ[]＝" + SaveLoadOnlySSS.packet.floatItems);

    }





    //引数を使った例
    void TestWait(AdvCommandSendMessageByName command)
    {
        StartCoroutine(CoWait(command));
    }

    //command.IsWaitでコマンド終了待ちも切り替えられる
    IEnumerator CoWait(AdvCommandSendMessageByName command)
    {
        command.IsWait = true;

        float time = command.ParseCellOptional<float>(AdvColumnName.Arg3, 0);
        while (true)
        {
            ////Debug.Log(time);
            time -= Time.deltaTime;
            if (time <= 0) break;
            yield return 0;
        }
        command.IsWait = false;
    }

    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        
    }
}
