﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveOrLoad : OverridableMonoBehaviour
{
    public GameObject maru_g;
    Transform maru_t;

    NameButtonKobetu NameButtonKobetu_SSS;

    GameObject NameButtonZentaiG;
    NameButtonZentai NameButtonZentaiSSS;//


    GameObject SaveLoadOnlyG;
    SaveLoadOnly SaveLoadOnlySSS;

    GameObject saveG;
    GameObject saveGmaruG;
    GameObject loadG;
    GameObject loadGmaruG;

    string myName;

    // Start is called before the first frame update
    void Start()
    {


        saveG = transform.Find("save").gameObject; //ここはできた。
        saveGmaruG = transform.Find("save/maru").gameObject; //ここはできた。
        loadG = transform.Find("load").gameObject; //ここはできた。
        loadGmaruG = transform.Find("load/maru").gameObject; //ここはできた。


        if (!SaveLoadOnlyG)
        {
            SaveLoadOnlyG = GameObject.Find("SaveLoadOnly");
            SaveLoadOnlySSS = SaveLoadOnlyG.GetComponent<SaveLoadOnly>();
        }

        if (!NameButtonZentaiG)//少し面倒か？　最低限必要か？
        {//NameButtonZentai
            NameButtonZentaiG = GameObject.Find("NameButtonZentai");//NameButtonZentai
            NameButtonZentaiSSS = NameButtonZentaiG.GetComponent<NameButtonZentai>();
        }

        if (!NameButtonKobetu_SSS) //同じゲームオブジェクト内の場合、これだけでOKかな？
        {
            NameButtonKobetu_SSS = GetComponent<NameButtonKobetu>();
        }

        





        simple();

        //デフォルトは「load」に。


    }

    public string hanbetu;
    public void simple()
    {
        if (ES2.Exists("stringSaveOrLoad?tag=string"))//newロード
        {
            hanbetu = ES2.Load<string>("stringSaveOrLoad?tag=string");
        }
        else
        {
            hanbetu = "load";
            ES2.Save(hanbetu, "stringSaveOrLoad?tag=string");
        }

        if (hanbetu == "save")
        {
            saveGmaruG.SetActive(true);
            loadGmaruG.SetActive(false);
        }
        else
        {
            saveGmaruG.SetActive(false);
            loadGmaruG.SetActive(true);

        }



    }


}

