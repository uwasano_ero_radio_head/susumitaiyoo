﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Animations;

public class tera : MonoBehaviour
{

public class InsertAnimation : MonoBehaviour
{
    private PlayableGraph graph;
    private AnimationPlayableOutput output;
    private Playable playable;

    [SerializeField] AnimationCurve curve = null;

    void Awake()
    {
        graph = PlayableGraph.Create();
        output = AnimationPlayableOutput.Create(graph, name, GetComponent<Animator>());
        output.SetWeight(0);
    }

    void OnDestroy()
    {
        graph.Destroy();
    }

    public IEnumerator Play(float time, AnimationClip animClip)
    {
        if (playable.IsValid())
            playable.Destroy();

        // アニメーションのセットアップ
        playable = AnimationClipPlayable.Create(graph, animClip);
        output.SetSourcePlayable(playable);

        graph.Play();

        // AnimatorからPlayableへ遷移
        for (float endTime = Time.timeSinceLevelLoad + time;
            endTime > Time.timeSinceLevelLoad;)
        {
            float diff = 1 - (endTime - Time.timeSinceLevelLoad) / time;
            output.SetWeight(curve.Evaluate(diff));
            yield return null;
        }
        output.SetWeight(1);

        // PlayableからAnimatorへ
        // AnimationClipPlayable再生中はAnimationControllerは止めておく
        GetComponent<Animator>().playableGraph.Stop();
        yield return new WaitForSeconds(animClip.length - time * 2);
        GetComponent<Animator>().playableGraph.Play();

        // PlayableからAnimatorへ
        for (float endTime = Time.timeSinceLevelLoad + time;
            endTime > Time.timeSinceLevelLoad;)
        {
            float diff = (endTime - Time.timeSinceLevelLoad) / time;
            output.SetWeight(curve.Evaluate(diff));
            yield return null;
        }
        output.SetWeight(0);

        // アニメーションのクリーンナップ
        playable.Destroy();
        graph.Stop();
    }
}
}
