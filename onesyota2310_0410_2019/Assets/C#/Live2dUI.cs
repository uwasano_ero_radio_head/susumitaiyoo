﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using Live2D.Cubism.Framework.Raycasting;

using Utage;
using Live2D.Cubism.Framework;
using Live2D.Cubism.Core;


public class Live2dUI : MonoBehaviour
{
    public Packet packet;
    [System.Serializable]
    public class Packet
    {
        //使い方わからん
        public int i;
        public bool b;
        public string L_myname;
    }

    //大文字小文字問題に注意

    [System.NonSerialized]
    public int ui_np_P;//これってなんなんだ？
    Camera cam;

    GameObject SaveLoadOnlyG;
    SaveLoadOnly SaveLoadOnlySSS;


    GameObject SaveBool_G;
    SaveBool SaveBool_SSS;

    GameObject UtageSeigyoG;
    UtageSeigyo UtageSeigyoSSS;

    GameObject GetLive2dModel_G;
    GetLive2dModel GetLive2dModel_SSS;//そもそもこれだけで動くの？

    void Start()
    {

        //Transform.Findを使った場合
        //例１）Transform target = this.transform.Find(“Parent_3 / target_2”);
        //例２）GameObject target = this.transform.Find(“Parent_3 / target_2”).gameObject;

        cam = Camera.main;

        if (!SaveLoadOnlyG)
        {
            SaveLoadOnlyG = GameObject.Find("SaveLoadOnly");
            SaveLoadOnlySSS = SaveLoadOnlyG.GetComponent<SaveLoadOnly>();
        }

        // if (!SaveBool_G)
        // {
        // SaveBool_G = GameObject.Find("SaveBool");
        // SaveBool_SSS = SaveBool_G.GetComponent<SaveBool>();
        //   }


        if (!UtageSeigyoG)//少し面倒か？　最低限必要か？
        {
            UtageSeigyoG = GameObject.Find("UtageSeigyo");
            UtageSeigyoSSS = UtageSeigyoG.GetComponent<UtageSeigyo>();
        }

        if (!GetLive2dModel_G)//少し面倒か？　最低限必要か？
        {
            GetLive2dModel_G = GameObject.Find("GetLive2dModel");
            GetLive2dModel_SSS = GetLive2dModel_G.GetComponent<GetLive2dModel>();//ここでエラーでるのなんなの
        }
        //ここではライブ２ｄを指定する必要はないのか！
        //Transform _trans = transform.Find("Hogehoge");
        //GameObject _obj = transform.gameObject;
        //live2dtestG = transform.Find("ka-ma_onesyota_sex_test0127_0411_2019").gameObject;
        //live2dtest = live2dtestG.GetComponent<CubismModel>();
        //live2dtest=transform.Find("ka-ma_onesyota_sex_test0127_0411_2019").gameObject;

        easySavedayo();
    }

    void easySavedayo()
    {
        //SaveLoadOnlySSS.packet.floatItems[0] = 1;
       // SaveLoadOnlySSS.packet.floatItems[1] = 1028;
       // SaveLoadOnlySSS.packet.floatItems[2] = 9837;//テスト

    }

    private void Update()
    {
        // Return early in case of no user interaction.
        if (!Input.GetMouseButtonDown(0))
        {
            return;
        }


        var raycaster = GetComponent<CubismRaycaster>();
        // Get up to 4 results of collision detection.
        var results = new CubismRaycastHit[2];


        // Cast ray from pointer position.
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var hitCount = raycaster.Raycast(ray, results);


        // Show results.
        var resultsText = hitCount.ToString();
        for (var i = 0; i < hitCount; i++)
        {
            resultsText += "\n" + results[i].Drawable.name;
            ////名前の取得
            packet.L_myname = results[i].Drawable.name;

            Debug.Log("results[i].Drawable.name = " + results[i].Drawable.name);
            Debug.Log("packet.L_myname = " + packet.L_myname);
        }
        test();//live2dをUIにする場合のテスト。でもあんま意味ないかも…。
    }

    void test(){
        //live2dulのテスト。でもこれlive2dじゃないほうがいいな…しまった無駄な手間だったか？
        if (packet.L_myname == "ArtMesh6")
        {
            bool a = true;//セーブ
            ui_np(0);

            SaveLoadOnlySSS.Do_Save_Bool(a, packet.L_myname);
            //boolのセーブ
            //これをとりあえずボタン系に置けばいいのかな？

            SaveLoadOnlySSS.Do_Load_Bool(a, packet.L_myname);
            //当たり判定付きArtMeshを連番にして　 for (var i = 0; i < hitCount; i++)みたいなやつで一気に処理する必要がある。
            //つまり当たり判定付きArtMeshは全部同じ名前、もしくは数字で管理する必要があるな

            //boolのロード
            //ロードに関してはスタート時？に全部ロードしなきゃいけないのでは？どうやる？
        }
        if (packet.L_myname == "ArtMesh5")
        {
            ui_np(1);
          
            //WaitCustomと一緒に使う。
            Debug.Log("スタートボタンを誉めた後");
            UtageSeigyoSSS.engineUtage.UiManager.IsInputTrigCustom = true;
            //これ超便利。これ気づいてなかったの悲しすぎじゃないか…0009_0503_2019
            UtageSeigyoSSS.engineUtage.JumpScenario("スタートボタンを誉めた後");//動いた
                                                                     // GetLive2dModel_SSS.live2d_G[0].SetActive(false);
        }
        if (packet.L_myname == "ArtMesh4")
        {
            ui_np(2);
        }
        if (packet.L_myname == "ArtMesh3")
        {
            // cam.transform.localPosition = new Vector3(-0.27f, 0.6f, 1.5f);//これでメインカメラ移動できることがわかった
            //  GetLive2dModel_SSS.live2d_G[0].SetActive(true);
            UtageSeigyoSSS.engineUtage.JumpScenario("表示するテスト");//動いた
        }
        if (packet.L_myname == "ArtMesh2")
        {
            // cam.transform.localPosition = new Vector3(2.373f, 0.6f, 1.5f);//シーン移動テスト
            //GetLive2dModel_SSS.live2d_G[0].SetActive(false);
            UtageSeigyoSSS.engineUtage.JumpScenario("消すテスト");//動いた
        }
        if (packet.L_myname == "ArtMesh1")
        {
            UtageSeigyoSSS.engineUtage.JumpScenario("ジャンプ先その１");//動いた
                                                                 //live2d_kesu_test.SetActive(true);
        }

    }


    private void ui_np(int a)
    {
        ui_np_P = a;
    }

    void KakuninDebug()
    {
        //確認用
    }


    void TestEndAttack()
    {
    }
}
