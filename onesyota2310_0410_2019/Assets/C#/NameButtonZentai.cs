﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

using Live2D.Cubism.Framework;
using Live2D.Cubism.Core;



public class NameButtonZentai : OverridableMonoBehaviour
{
    //これは全体のためのもの
    public GameObject AdvEngineG;
    public GameObject MessageWindowG;//宴のメッセージウィンドウ

    public List<string> stringList = new List<string>();    //string型のリスト

    GameObject PanelAnmakuG;
    public FadeInFadeOut FadeInFadeOutSSS;

    bool idouOn;
    string sayuu;
    GameObject UtageUguiMainGameG;
    UtageUguiMainGame UtageUguiMainGameSSS;
    GameObject MainGameG;
    GameObject CanvasAdvUIG;

    Camera cam;
    Transform camT;
    bool boolTouch;
    bool booln;
    GameObject clickedGameObject;
    public string myName;
    GameObject[] buttonG;
    GameObject[] newG;
    GameObject[] touchG;

    public int sean;//NameButtonKobetuで使われてる

    GameObject UtageSeigyoG;
    UtageSeigyo UtageSeigyoSSS;

    GameObject GetPosCamG;
    GetPosCam GetPosCamSSS;

    GameObject SaveLoadOnlyG;
    SaveLoadOnly SaveLoadOnlySSS;

    GameObject ParticleTapG;
    ParticleSystem ParticleTapP;


    GameObject GetLive2dModel_G;
    GetLive2dModel GetLive2dModel_SSS;//そもそもこれだけで動くの？
    



    void Start()
    {//ここでボタン軍を全部取得できればいいんだが…
     //load();
     //namaeGet();
     //クリックしたものの名前ゲットだからここでは出ない

       

        if (!AdvEngineG)
        {
            AdvEngineG = GameObject.Find("AdvEngine");
           MessageWindowG = AdvEngineG.transform.Find("UI").gameObject;
        }


        if (!GetLive2dModel_G)//少し面倒か？　最低限必要か？
        {
            GetLive2dModel_G = GameObject.Find("GetLive2dModel");
            GetLive2dModel_SSS = GetLive2dModel_G.GetComponent<GetLive2dModel>();//ここでエラーでるのなんなの
            //GetLive2dModel_III = GetLive2dModel_G.GetComponent<CubismParametersInspector>();//ここでエラーでるのなんなの
        }



        if (!UtageSeigyoG)//少し面倒か？　最低限必要か？
        {
            UtageSeigyoG = GameObject.Find("UtageSeigyo");
            UtageSeigyoSSS = UtageSeigyoG.GetComponent<UtageSeigyo>();
        }



        if (!PanelAnmakuG)//少し面倒か？　最低限必要か？
        {
            PanelAnmakuG = GameObject.Find("PanelAnmaku");
            FadeInFadeOutSSS = PanelAnmakuG.GetComponent<FadeInFadeOut>();
        }

        if (!ParticleTapG)//少し面倒か？　最低限必要か？
        {
            ParticleTapG = GameObject.Find("ParticleTap");
            ParticleTapP =  ParticleTapG.GetComponent<ParticleSystem>();
        }
      

        stringList.Add("StartPos");//スタート画面に戻ってこられるように

        cam = Camera.main;
        camT = cam.GetComponent<Transform>();

        if (!UtageUguiMainGameG)//少し面倒か？　最低限必要か？
        {
            CanvasAdvUIG = GameObject.Find("Canvas-AdvUI");
            MainGameG = CanvasAdvUIG.transform.Find("MainGame").gameObject;
          
            UtageUguiMainGameSSS = MainGameG.GetComponent<UtageUguiMainGame>();
        }

        if (!GetPosCamG)//少し面倒か？　最低限必要か？
        {
            GetPosCamG = GameObject.Find("GetPosCam");
            GetPosCamSSS = GetPosCamG.GetComponent<GetPosCam>();
        }
       if (!SaveLoadOnlyG)
       {
          SaveLoadOnlyG = GameObject.Find("SaveLoadOnly");
            SaveLoadOnlySSS = SaveLoadOnlyG.GetComponent<SaveLoadOnly>();
        }

        Debug.Log("存在確認=" + SaveLoadOnlySSS.packet);

        GoStart();
    }
    
  
    //public void GoReturn()//
    //{
    //    GetPosCamSSS.GoMyName(goReturnN0);
    //    繰り返しリターンを使いたくなる場合は？？
    //}

    public void GoSomeWhere(string myName)//
    {
        //ロード画面へ
        //ロードロード
        //myName = transform.name;//自分の名前をゲット
        string mgo = myName.Substring(0, 2);//最初の二文字取得


        string m2 = myName.Remove(0, 2);//最初の2文字消せてる
        if (myName== "TitleStart")
        {
            //UtageUguiMainGameSSS.title.OnTapStart();//エクセルの一番上に飛ぶ

         UtageSeigyoSSS.engineUtage.UiManager.IsInputTrigCustom = true;//utageが有効になるらしい
            //これ超便利。これ気づいてなかったの悲しすぎじゃないか…0009_0503_2019
            UtageSeigyoSSS.engineUtage.JumpScenario("スタートボタンを誉めた後");//動いた
            //
            myName = "EroPos";//代入された
            GetPosCamSSS.GoMyName(myName);//位置を取得
            stringList.Add(myName);

        }
        else if (myName == "TitleLoad")
        {
           
            myName = "LoadPos";//代入された
            GetPosCamSSS.GoMyName(myName);//位置を取得
            stringList.Add(myName);
        }
        else if (myName == "Config")
        {
            UtageUguiMainGameSSS.title.OnTapConfig();
            //myName = "ConfigPos";
            //GetPosCamSSS.GoMyName(myName);//位置を取得
            //stringList.Add(myName);

        }
        else if (myName == "Gallery")
        {
            UtageUguiMainGameSSS.title.OnTapGallery();

        }
        else if (myName == "save")
        {
            UtageSeigyoSSS.engineUtage.JumpScenario("セーブロード判別");

        }
        else if (myName == "load")
        {
            UtageSeigyoSSS.engineUtage.JumpScenario("セーブロード判別");

        }




        //複雑で良くないかもなこの方法…

        if (mgo == "Re")//最初の文字で判別することで名前の重複問題を解決した
        {
           
            //リターン処理これ難しいな～～
            int a=stringList.Count;
            Debug.Log("listの数 "+a);
            if (a>=2) {
                //最初にスタートが入力されてるので　一個移動したら二個になる
                GetPosCamSSS.GoMyName(stringList[a-2]);//位置を取得
                stringList.RemoveAt(a-1);
            }


        }
        else if (mgo == "GG") {
            //スタートの時だけの処理
          
            GetPosCamSSS.GoMyName("StartPos");//位置を取得
            stringList.Add("StartPos");
        }
        else if (mgo == "Go")//頭文字がGoかどうかを調べる
        {
         
            stringList.Add(m2);
            string m2mgo = m2.Substring(0, 2);//最初の二文字取得

            GetPosCamSSS.GoMyName(m2);//位置を取得
    

        }
    }

  


    public void GoLoad()//
    {
        GetPosCamSSS.GoMyName("LoadPos");//位置を取得
        stringList.Add("LoadPos");

        //Vector3 a = GetPosCamSSS.loadPostT.gameObject.transform.position;
       // camT.gameObject.transform.position = a;
    }
  
   
    public void GoStart()//
    {
        //UtageUguiMainGameSSS.Open();
        UtageUguiMainGameSSS.title.Open();
        //スタート画面へ
        sean = 1;
        Vector3 a = GetPosCamSSS.StartPosT.gameObject.transform.position;
        camT.gameObject.transform.position = a;
    }

    void LateUpdate()
    {
        //GetLive2dModel_SSS._paramAngleX.Value = 30;
    }
    //bool ositeru;
    //void ositeruKaihou()
    //{
    //    if (ositeru) {

    //        var pos = cam.ScreenToWorldPoint(Input.mousePosition + cam.transform.forward * 10);
    //        ParticleTapG.transform.position = pos;

    //        ositeru = false;
    //        //ParticleTapP.Emit(5);
    //    }
    //}
    int jikankeikaTest;
    public override void UpdateMe()//全体
    {
    
      

        if (idouOn)//移動オンになってる場合
        {
            Debug.Log("伊集院AAA1021_0605_2019");
            Vector3 a = camT.gameObject.transform.position;//位置をゲット
            if (sayuu=="migi")
            {
                a.x++;

                camT.gameObject.transform.position = a;
            }
            else if(sayuu == "hidari")
            {
                a.x--;

                camT.gameObject.transform.position = a;
            }

          
        }
        //if (ositeru)
        //{
        //    //PartivlePositionGet2();//ドラッグ中取得
        //    //keisanX();
        //    //keisanY();
        //    jikankeikaTest++;
        //    //Debug.Log("押した瞬間化どうかテスト＝" + jikankeikaTest);
        //}

        if (Input.GetMouseButtonDown(0))//全体 //クリック時の瞬間
        {

           

            pos1.x = 0;
            pos2.x = 0;
            sayuu = "";
            jikankeikaTest = 0;
       
            //↑初期化


            //ositeru = true;
            //Invoke("ositeruKaihou", 1);
            PartivlePositionGet1();//起点取得
            //UtageSeigyoSSS.engineUtage.UiManager.IsInputTrigCustom = true;//utageが有効になるらしい



            //これは
            namaeGet();//名前ゲットしてからじゃないとちゃんとロードされない。凡ミス…0226_0430_2019
           load("touch_");//必要
            //個別の方とデータをそろえるためかな？
            //個別の方とロードとかでやり取りしててややこしい！！

            //Debug.Log("過去の数値" + b);


            //切り替えは個別の方だけでやることにした2300_0430_2019

            //全体
            ES2.Save(boolTouch, "new_" + myName + "bool?tag=bool");//各種名前のセーブデータゲット
            //newはデータが存在しているかどうかで判定している。
            ES2.Save(boolTouch, "touch_" + myName + "bool?tag=bool");//各種名前のセーブデータゲット
            //touchは一応関係ある。
            //なぜtrueに戻らないのか？
           load("touch_");//二個あるが必要

            //ここで保存したデータを元にして表示変化が起きている
            //Debug.Log("現在の数値"+ b);


        }else if (Input.GetMouseButtonUp(0))
        {

            PartivlePositionGet2();//終点取得　マウス離した

            pageIdou();
            // Debug.Log("なんで連続で移動するのか？jikankeikaTest=" + jikankeikaTest);

            //ositeruKaihou();
            //Debug.Log("jikankeikaTest=" + jikankeikaTest);
            jikankeikaTest = 0;
            //Debug.Log("なんで連続で移動するのか？jikankeikaTest=" + jikankeikaTest);
        }


    }

    public Vector3 pos1;
        public Vector3 pos2;

    void PartivlePositionGet2()//パーティクルの位置変更
    {
        var pos = cam.ScreenToWorldPoint(Input.mousePosition + cam.transform.forward * 10);
        ParticleTapG.transform.position = pos;
        pos2 = pos;
        ParticleTapP.Emit(1);
        Debug.Log("離した　pos2 = "+ pos.x);
    }

    int seanidouTest;//
    void PartivlePositionGet1()//パーティクルの位置変更
    {
        //これらをおっぱい触った時だけに設定したりするといけるじゃん
        var pos = cam.ScreenToWorldPoint(Input.mousePosition + cam.transform.forward * 10);
        ParticleTapG.transform.position = pos;
        pos1 = pos;
        ParticleTapP.Emit(1);
        Debug.Log("押した pos1 = " + pos.x);
    }
    int bairitu = 15;
    void keisanX()
    { float a;
        float b;
        float sa;
        a = Mathf.Abs(pos1.x);
        b = Mathf.Abs(pos2.x);
        sa=a - b;
        sa = sa * bairitu;

        
        if (sa>0)
        {
            //Debug.Log("右（すすむ） =" + sa);
      
        }
        else
        {
          
            //Debug.Log("左（もどる） =" + sa);
        }

    }

    void pageIdou()
    {
        float a;
        float b;
        float c;
        float d = 0.1f;//差の制限
        float sa;
        a = pos1.x;
        b = pos2.x;
        //sa = a - b;
        //sa = sa * bairitu;
        if (a==b)
        {
            sayuu = "";
            //同じ値
        }
        else if (a < b)
        {
            c = b - a;
            if(c>d)
            {

                //Debug.Log("a"+a+"b"+b+"="+"右（すすむ）→"+ seanidouTest);
                seanidouTest++;
                if (seanidouTest > 4)
                {
                    seanidouTest = 0;
                }
                idouOn = true;
                sayuu = "migi";

                Invoke("PageIdouNakami", 0.1f);

            }



           
           
        }
        else
        {
            c = a - b;
            if (c > d)
            {

                //Debug.Log("a" + a + "b" + b + "=" + "左（もどる）→" + seanidouTest);
                seanidouTest--;
                if (seanidouTest <= 0)
                {
                    seanidouTest = 0;
                }
                idouOn = true;
                sayuu = "hidari";

                Invoke("PageIdouNakami", 0.1f);
            }
          
        }

        var pos = cam.ScreenToWorldPoint(Input.mousePosition + cam.transform.forward * 10);
        pos1 = pos;
            pos2 = pos;
        //Debug.Log("移動！");
        ParticleTapP.Emit(1);
        //Debug.Log("sayuu="+sayuu+""+"a="+a+"b="+b);

    }

    void PageIdouNakami()
    {
        GetPosCamSSS.IdouHontai(GetPosCamSSS.GameObjectList[seanidouTest].name);
        idouOn = false;//少しだけ画面が動いてから移動する処理
        //Invoke("ositeruKaihou", 0.1f);
       
    }


    void keisanY()
    {
        float a;
        float b;
        float sa;
        a = Mathf.Abs(pos1.y);
        b = Mathf.Abs(pos2.y);
        sa = a - b;
        sa = sa * bairitu;
        if (sa > 0)
        {
            //debug.Log("上 =" + sa);
        }
        else
        {
           //debug.Log("下 =" + sa);
        }

    }
    //これでいけるんじゃね？？？


    public bool saveAndLoadMode;
    void namaeGet()//全体
    {
        if (saveAndLoadMode==false) {//セーブモードの時にはここは実行されない
            //マップモードでしかここを使用しないなどの制限が必要

            myName = "";//最初に消してる
                        //Debug.Log("名前消してる");
            clickedGameObject = null;//一旦nullを入れて新しく取得してるのか…
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit2d = Physics2D.Raycast((Vector2)ray.origin, (Vector2)ray.direction);
            if (hit2d)//←重要すぎた…
            {
                clickedGameObject = hit2d.transform.gameObject;
                myName = clickedGameObject.name;//名前取得
                //ここの処理の意味を理解してなかったからバグがなかなか直せなかった…AAA1304_0520_2019
            }



            //Debug.Log("hit2d=" + myName);

        }
    }


    void load(string a)//全体
    {
        //これ必要か？必要だった1253_0503_2019
        //a = "touch_";
        if (ES2.Exists(a + myName + "bool?tag=bool"))//ロード
        {
            boolTouch = ES2.Load<bool>(a + myName + "bool?tag=bool");
        }
    }
    //static string kaigyou = "\n";
    static string kaigyou = " ";//改行を無効にしてみた
    static string kugiri = "/";
    static string kugiri2 = ":";

    public string hidukeT = "";

    public void hidukeLoad(string myName)
    {
        if (SaveLoadOnlySSS.packet.floatItems!=null) {

           

            hidukeT = "";
            hidukeT += myName + " " + kaigyou;
            hidukeT += SaveLoadOnlySSS.packet.floatItems[0] + kugiri;
            hidukeT += SaveLoadOnlySSS.packet.floatItems[1] + kugiri;
            hidukeT += SaveLoadOnlySSS.packet.floatItems[2] + "   ";
            //
            hidukeT += SaveLoadOnlySSS.packet.floatItems[3] + kugiri2;
            hidukeT += SaveLoadOnlySSS.packet.floatItems[4] + kugiri2;
            hidukeT += SaveLoadOnlySSS.packet.floatItems[5] + "";
           //Debug.Log("存在確認=" + hidukeT);
        }

    }


}

