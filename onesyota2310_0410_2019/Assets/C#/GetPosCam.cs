﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetPosCam : OverridableMonoBehaviour
{//位置取得専門

    public List<GameObject> GameObjectList = new List<GameObject>();    //GameObject型のリスト


    Camera cam;
    Transform camT;
    GameObject StartPosG;
    public Transform StartPosT;
    GameObject LoadPosG;
    public Transform loadPostT;
    GameObject PassPosG;
    public Transform PassPosT;
    GameObject SomePosG;
    public Transform SomePosT;
   public string myNameFinal;
    GameObject UtageUguiMainGameG;
    UtageUguiMainGame UtageUguiMainGameSSS;
    GameObject MainGameG;
    GameObject CanvasAdvUIG;

    GameObject PanelAnmakuG;
    public FadeInFadeOut FadeInFadeOutSSS;


    public override void UpdateMe()//全体
    {
        if (FadeInFadeOutSSS.isFadeOut)
        {

            FadeInFadeOutSSS.StartFadeOnOff(myNameFinal);//暗幕処理
            //ここのmyNameFinalが欲しいやつを取得できていない。

        }
    }


        // Start is called before the first frame update
        void Start()
    {
        // 自分の１つ下の階層にオブジェクトがいくつあるか
        int count = transform.childCount;
        // 自分の１つ下の階層のオブジェクトを全て取得
        for (int i = 0; i < count; i++)
        {
            Transform child = transform.GetChild(i);
            // 取得したオブジェクトの名前を表示
            GameObjectList.Add(child.gameObject);
            //Debug.Log("一括取得 = " + child.name);
        }

       


        if (!PanelAnmakuG)//少し面倒か？　最低限必要か？
        {
            PanelAnmakuG = GameObject.Find("PanelAnmaku");
            FadeInFadeOutSSS = PanelAnmakuG.GetComponent<FadeInFadeOut>();
        }

        if (!UtageUguiMainGameG)//少し面倒か？　最低限必要か？
        {
            CanvasAdvUIG = GameObject.Find("Canvas-AdvUI");
            MainGameG = CanvasAdvUIG.transform.Find("MainGame").gameObject;

            UtageUguiMainGameSSS = MainGameG.GetComponent<UtageUguiMainGame>();
        }

        cam = Camera.main;
        camT = cam.GetComponent<Transform>();

        StartPosG = transform.Find("StartPos").gameObject;
        StartPosT = StartPosG.GetComponent<Transform>();

        LoadPosG = transform.Find("LoadPos").gameObject;
        loadPostT = LoadPosG.GetComponent<Transform>();

        PassPosG = transform.Find("PassPos").gameObject;
        PassPosT = PassPosG.GetComponent<Transform>();
    }

    public void GoMyName2(string myName)
    {
      
        if (myName == null)
        {
            Debug.Log("「" + myName + "」はnullです！");
        }
        else {

            //黒くなる

            //黒くなってから移動

            IdouHontai(myName);//移動処理の本体

            //明るくなる
        }

    }


    public void GoMyName(string myName)//暗幕がかかって消える処理
    {
        if (myName == null)
        {
        }
        else
        {
            myNameFinal = myName;//連続処理されるmyName用の変数に代入
            FadeInFadeOutSSS.StartFFF();

            }
          
        
    }


    public void IdouHontai(string myName)
    {
        //移動処理の本体
        //Debug.Log("本体のmyName = " + myName);
        SomePosG = transform.Find(myName).gameObject;
        SomePosT = SomePosG.GetComponent<Transform>();
        Vector3 a = SomePosT.gameObject.transform.position;//位置をゲット
        camT.gameObject.transform.position = a;
        //

    }









}
