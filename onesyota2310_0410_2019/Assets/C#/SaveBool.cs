﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using Live2D.Cubism.Framework.Raycasting;

using Utage;
using Live2D.Cubism.Framework;
using Live2D.Cubism.Core;



public class SaveBool : MonoBehaviour
{
    // Start is called before the first frame update


    Camera cam;
    bool pabool;
    Transform parent;
    void Start()
    {
        parent = transform.root;//親を取得
        //packet.booln = new bool a;//必ず上にないといけない
        Do_Load_Booln();//こっちでエラーなのかな？
      
        Debug.Log("リモコンをひろってきた！！"+parent.name);
    }

    private void Update()
    {
        // Return early in case of no user interaction.
        if (!Input.GetMouseButtonDown(0))
        {
            return;
        }


        var raycaster = GetComponent<CubismRaycaster>();
        // Get up to 4 results of collision detection.
        var results = new CubismRaycastHit[2];


        // Cast ray from pointer position.
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var hitCount = raycaster.Raycast(ray, results);


        // Show results.
        var resultsText = hitCount.ToString();
        
    }

            public void Do_Save_Booln()
    {

        Debug.Log("Saveリモコンをひろってきた！！" + parent.name);
        ES2.Save(pabool, "myFile_" + parent.name + ".bool?tag=bool");//savekanriを書き分けることで複数のセーブデータを管理することができる 

        Debug.Log("Saveリモコンをひろってきた！！" + parent.name);

    }
    public void Do_Load_Booln()
    {
        if (ES2.Exists("myFile_" + parent.name + ".bool?tag=bool"))//ロード
        {

            Debug.Log("データがあれば？" + parent.name);
            pabool = ES2.Load<bool>("myFile_" + parent.name + ".bool?tag=bool");
            //データを消すときのロード

            Debug.Log("Loadリモコンをひろってきた！！" + parent.name);
        }

    }

    
}
