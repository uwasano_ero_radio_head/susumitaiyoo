﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //パネルのイメージを操作するのに必要


public class FadeInFadeOut : OverridableMonoBehaviour
{
    GameObject GetPosCamG;
    GetPosCam GetPosCamSSS;

    //インスペクタから見えなくする上に初期化されるらしい
    [System.NonSerialized]
    public float fadeSpeed = 0.2f;
    [System.NonSerialized] //透明度が変わるスピードを管理
    public float red, green, blue, alfa;   //パネルの色、不透明度を管理

    public bool isFadeOut = false;  //フェードアウト処理の開始、完了を管理するフラグ

    GameObject PanelAnmakuG;
    public Image fadeImageI;                //透明度を変更するパネルのイメージ

    void Start()
    {
        if (!PanelAnmakuG)//少し面倒か？　最低限必要か？
        {
            //PanelAnmakuG = GameObject.Find("PanelAnmaku");
            fadeImageI = this.GetComponent<Image>();
        }
        if (!GetPosCamG)//少し面倒か？　最低限必要か？
        {
            GetPosCamG = GameObject.Find("GetPosCam");
            GetPosCamSSS = GetPosCamG.GetComponent<GetPosCam>();
        }


        fadeImageI = GetComponent<Image>();
        red = fadeImageI.color.r;
        green = fadeImageI.color.g;
        blue = fadeImageI.color.b;
        alfa = fadeImageI.color.a;
    }

    public override void UpdateMe()//全体
    {
       
    }
    public int oken=0;
    public void StartFadeOnOff(string myName)//暗幕がかかって消える処理
    {
        //これが現在連続処理されているもの。
        if (oken==0) {
            fadeImageI.enabled = true;  // a)パネルの表示をオンにする
            alfa += fadeSpeed;         // b)不透明度を徐々にあげる
            SetAlpha();               // c)変更した透明度をパネルに反映する
            if (alfa >= 1)
            {             // d)完全に不透明になったら処理を抜ける
                oken = 1;
                GetPosCamSSS.IdouHontai(myName);
            }
        }
        else if(oken==1)
        {
            alfa -= fadeSpeed;         // b)不透明度を徐々にあげる
            SetAlpha();               // c)変更した透明度をパネルに反映する
            if (alfa <= 0)
            {             // d)完全に不透明になったら処理を抜ける
                isFadeOut = false;
                oken = 2;
            }
        }
    }

    public void StartFFF()//開始処理
    {
       oken = 0;
       isFadeOut = true;

    }



    public void SetAlpha()
    {
        fadeImageI.color = new Color(red, green, blue, alfa);
    }
}
