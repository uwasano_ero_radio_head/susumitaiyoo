﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEditor;
using Utage;
using UnityEngine;
using Utage;
using System.Collections;
/// <summary>
/// ADV用SendMessageByNameコマンドから送られたメッセージを受け取る処理のサンプル
/// </summary>
[AddComponentMenu("Utage/ADV/Examples/SendMessageByName")]
public class SaveTextManagerKobetu : OverridableMonoBehaviour
{


    GameObject newG;
    Transform new_t;
    GameObject touchG;
    Transform touchT;
    GameObject loveG;
    string stringSaveOrLoad;
    string myName;



    NameButtonKobetu NameButtonKobetu_SSS;

    GameObject SaveLoadOnlyG;
    SaveLoadOnly SaveLoadOnlySSS;

    SpriteRenderer thisSP;



    float f;

    int kazu = 5;

    GameObject[] saveDataTextGs;


    GameObject saveDataTextG;
    TMPro.TextMeshPro saveDataTextT;

    GameObject UtageSendMessageByNameG;
    UtageSendMessageByName UtageSendMessageByNameSSS;


    GameObject NameButtonZentaiG;
    NameButtonZentai NameButtonZentaiSSS;//そもそもこれだけで動くの？


    GameObject UtageSeigyoG;
    UtageSeigyo UtageSeigyoSSS;




    // Start is called before the first frame update
    void Start()
    {

        //if (!PassWordExportG)
        //{
        //    PassWordExportG = GameObject.Find("PassWordExport");
        //    PassWordExportSSS = PassWordExportG.GetComponent<PassWordExport>();
        //}
        //if (!PassWordImportG)
        //{
        //    PassWordImportG = GameObject.Find("PassWordImport");
        //    PassWordImportSSS = PassWordImportG.GetComponent<PassWordImport>();
        //}



        if (!UtageSeigyoG)//少し面倒か？　最低限必要か？
        {
            UtageSeigyoG = GameObject.Find("UtageSeigyo");
            UtageSeigyoSSS = UtageSeigyoG.GetComponent<UtageSeigyo>();
        }

        if (!NameButtonZentaiG)//少し面倒か？　最低限必要か？
        {//NameButtonZentai
            NameButtonZentaiG = GameObject.Find("NameButtonZentai");//NameButtonZentai
            NameButtonZentaiSSS = NameButtonZentaiG.GetComponent<NameButtonZentai>();
        }

        if (!SaveLoadOnlyG)
        {
            SaveLoadOnlyG = GameObject.Find("SaveLoadOnly");
            SaveLoadOnlySSS = SaveLoadOnlyG.GetComponent<SaveLoadOnly>();
        }

        if (!UtageSendMessageByNameG)
        {
            UtageSendMessageByNameG = GameObject.Find("UtageSendMessageByName");
            UtageSendMessageByNameSSS = UtageSendMessageByNameG.GetComponent<UtageSendMessageByName>();
        }

        if (!NameButtonKobetu_SSS) //同じゲームオブジェクト内の場合、これだけでOKかな？
        {
            NameButtonKobetu_SSS = GetComponent<NameButtonKobetu>();
        }
        saveDataTextG = transform.Find("Text").gameObject; //ここはできた。
        saveDataTextT = saveDataTextG.GetComponent<TMPro.TextMeshPro>();

        thisSP = this.GetComponent<SpriteRenderer>();
        //Sprite a = 



        myName = transform.name;//自分の名前をゲット
        int len = myName.Length;//最初の文字を消した状態で文字数を取得
        string m = myName.Remove(len - 1);//最後の文字消せてる
        string m2 = m.Remove(0, 2);//最初の文字消せてる

        thisSP.sprite = Resources.Load<Sprite>("namber/" + m2);
        //DataLoad();
        Invoke("DataLoad", 0.1f);
        //Invoke("DoSaveKakunin", 0.1f);//セーブしてるいみがわからん



    }

    public override void UpdateMe()//個別
    {//updatemanagerから呼ばれるようになった
        if (Input.GetMouseButtonDown(0))
        {

            if (myName == NameButtonZentaiSSS.myName && UtageSendMessageByNameSSS.kakuninZumi == true)
            {
                Invoke("aaaSave", 0.1f);
                //aaaSave();

                NameButtonZentaiSSS.myName = "";//myNameを初期化
                //myNameに頼る害、ノウハウが少なくて対処できない場合はもっとシンプルな方法を考えること。AAA0119_0521_2019
            }
        }
    }


    bool b;
    public void aaaSave()
    {
        //宴で何かの数値を出して　ここでセーブを実行することにするか…
        if (UtageSendMessageByNameSSS.kakuninZumi)//自分の名前を判別できてないみたいだな…
        {
            UtageSendMessageByNameSSS.kakuninZumi = false;
            //Invoke("daiji", 0.2f);//ここでセーブが実行される。宴を呼ぶ→確認→宴から実行？
            Debug.Log("aaaSave myName = " + myName);
            daiji(myName);

            NameButtonZentaiSSS.saveAndLoadMode = false;

        }
    }








    public void DoSaveKakunin()
    {
        Debug.Log("DoSaveKakunin");
        stringSaveOrLoad = ES2.Load<string>("stringSaveOrLoad?tag=string");//セーブかロードの区別
        NameButtonZentaiSSS.saveAndLoadMode = true;

        if (stringSaveOrLoad == "save")
        {
            UtageSeigyoSSS.engineUtage.JumpScenario("セーブ確認");//動いた

            //SaveHontai();//宴から呼ぶことにした

        }
        else
        {
            if (ES2.Exists(myName))//ロード（配列）
            {
                UtageSeigyoSSS.engineUtage.JumpScenario("ロード確認");//動いた

                //LoadHontai();//宴から呼ぶことにした
            }
            else
            {
                Debug.Log("データがありません！" + myName);
            }

        }

        //DataLoad();





    }

    static string kaigyou = "\n";
    static string kugiri = "/";
    static string kugiri2 = ":";


    void DataLoad()
    {
        if (ES2.Exists(myName))//ロード（配列）
        {
            //Debug.Log("データある" + myName);
            SaveLoadOnlySSS.packet.floatItems = ES2.LoadArray<float>(myName);//名前直接ロード。

            NameButtonZentaiSSS.hidukeLoad(myName);//これを使うと現在時刻になってしまうのでは？
            saveDataTextT.text = NameButtonZentaiSSS.hidukeT;//

        }
        else
        {
            //Debug.Log("データない" + myName);
            saveDataTextT.text = myName + kaigyou + "No Data";

        }


    }





    void daiji(string myName)
    {

        if (stringSaveOrLoad == "save")
        {
            Debug.Log("セーブします！" + myName + "/");
            UtageSendMessageByNameSSS.hidukeSave();//最新の日付にする

            ES2.Save(SaveLoadOnlySSS.packet.floatItems, myName);//各種名前のセーブデータゲット
            SaveLoadOnlySSS.Export();//パスワード出力
            Debug.Log("パスワード出力！" + myName);
        }
        else
        {
            Debug.Log("ロードします！ = " + myName);
            SaveLoadOnlySSS.packet.floatItems = ES2.LoadArray<float>(myName);//名前直接ロード。
            SaveLoadOnlySSS.Export();//パスワード出力
            Debug.Log("パスワード出力！" + myName);
        }
        DataLoad();//日付の表示

    }



    void anan()//これ不要？
    {
        for (int i = 0; i < kazu; i++)
        {
            saveDataTextGs[i] = transform.Find("saveDataText (" + i + ")").gameObject;
        }
    }




}
