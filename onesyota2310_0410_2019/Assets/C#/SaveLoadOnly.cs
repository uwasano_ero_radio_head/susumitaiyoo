﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveLoadOnly : MonoBehaviour
{
    // Start is called before the first frame update

    GameObject PassWordExportG;
    InputField PassWordExportGI;

    GameObject PassWordImportG;
    InputField PassWordImportGI;

GameObject NameButtonZentaiG;
    NameButtonZentai NameButtonZentaiSSS;//

    public Packet packet;
    [System.Serializable]
    public class Packet
    {
        //public string[] names;
        public string name;
        public int itemCount;
        public bool booln;
        public int[] intItems;//必ず上にないといけない];
        public float[] floatItems;//必ず上にないといけない];
        public string[] stringItems;
        // セーブデータのバージョンを持っていた方が
        // セーブデータの内容をアップデートする際に便利になることが多い
        public int version;
        //この中身が保存されるわけか…
        //宴のスクリプトに入れるか
        //これ全部保存できるの？？？

    }
    static int saveKazus = 401;//セーブデータの数
    [System.NonSerialized]
    public int saveKazu = saveKazus;//セーブデータの数
    public bool a;

  


    void Start()
    {



        if (!PassWordExportG)
        {
            PassWordExportG = GameObject.Find("PassWordExport");
            PassWordExportGI = PassWordExportG.GetComponent<InputField>();
        }

        if (!PassWordImportG)
        {
            PassWordImportG = GameObject.Find("PassWordImport");
            PassWordImportGI = PassWordImportG.GetComponent<InputField>();
        }



        // packet.items = new int[saveKazu];//必ず上にないといけない
        packet.floatItems = new float[saveKazu];//必ず上にないといけない


    }



  






    // Update is called once per frame

    public void Do_Save_Bool(bool booln_1,string myName)
    {
        //この辺つかわないかも。かえってこんがらがるかも？？？
        ES2.Save(booln_1, "myFile_" + myName + "bool?tag=bool");//myNameを書き分けることで複数のセーブデータを管理することができる 

               Debug.Log("AこれどうなったんだよSave" + booln_1);

    }
    public void Do_Load_Bool(bool booln_1, string myName)
    {


        if (ES2.Exists( "myFile_" + myName + "bool?tag=bool"))//ロード
        {
            booln_1 = ES2.Load<bool>("myFile_" + myName + "bool?tag=bool");
            Debug.Log("BこれどうなったんだよLoad" + booln_1);
        }

    }



    public void DoSaveintItems(string myName)
    {

        ES2.Save(packet.intItems, "myFile_" + myName + "int?tag=intArray");//myNameを書き分けることで複数のセーブデータを管理することができる 

    }
    public void DoLoadintItems(string myName)
    {
        if (ES2.Exists("myFile.txt?tag=intArray"))//ロード
        {
            packet.intItems = ES2.LoadArray<int>("myFile" + myName + ".txt?tag=intArray");
            //データを消すときのロード
        }

    }

    public void DoSavefloatItems(float[]a,float myName)
    {

        ES2.Save(packet.floatItems, "myFile_" + myName + "float?tag=floatArray");//myNameを書き分けることで複数のセーブデータを管理することができる 

    }
    public void DoLoadfloatItems(float myName)
    {
        if (ES2.Exists("myFile.txt?tag=floatArray"))//ロード
        {
            packet.floatItems = ES2.LoadArray<float>("myFile" + myName + ".txt?tag=floatArray");
            //データを消すときのロード
            Debug.Log("ロードここだよ！！");
        }

    }



    public void Import()
    {
        //これを最初に押しちゃうと動かなくなっちゃう
        //文字数チェックしてから下を実行するか

        Debug.Log("Hello Hello");
        Debug.Log("Hello " + PassWordImportGI.text);
        var base64 = PassWordImportGI.text;
        //文字数を取得
        int len = base64.Length;
        if (len > 300)//300文字以上
        {
            var utf8bytes = System.Convert.FromBase64String(base64);
            var json = System.Text.Encoding.UTF8.GetString(utf8bytes);
          packet = JsonUtility.FromJson<Packet>(json);
            //これだとロードできないんじゃね？
            PassWordImportGI.text = "インポートが成功しました";
            ES2.Save(packet.floatItems, "myFile.txt?tag=floatArray");


            //パスワードがまちがっていた場合の処理どうする？


            if (ES2.Exists("myFile.txt?tag=floatArray"))
            {
               packet.floatItems = ES2.LoadArray<float>("myFile.txt?tag=floatArray");
            }
            Debug.Log("Hello" + packet.floatItems[0]);

        }
        else
        {
            PassWordImportGI.text = "パスワードが違います";
        }


    }

    public void Export()
    {

        if (ES2.Exists("myFile_float.txt?tag=floatArray"))
        {
          packet.floatItems = ES2.LoadArray<float>("myFile_float.txt?tag=floatArray");
            //ロードした
        }
        var json = JsonUtility.ToJson(packet);
        var utf8bytes = System.Text.Encoding.UTF8.GetBytes(json);
        var base64 = System.Convert.ToBase64String(utf8bytes);
        //this.inputfield = base64;
        PassWordExportGI.text = base64;
        //Debug.Log("guruExport" + packet.floatItems[0]);


    }

}
